provider "aws" {
  region = "us-west-1"
}

# Specify the EC2 details
resource "aws_instance" "example" {
  ami           = "ami-0b99c7725b9484f9e"
  instance_type = "t2.micro"
}
